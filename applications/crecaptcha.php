<?php

if (!function_exists("_recaptcha_qsencode")) {
    include path::plugins("recaptcha/recaptchalib.php");
}

class crecaptcha extends app {

    static function form() {
        ob_start();
        $publickey = "6LcXtMsSAAAAAPjUqlkutojthMjDI_kKoyFDetc3"; // you got this from the signup page
        echo recaptcha_get_html($publickey);
        return ob_get_clean();
    }

    static function validacao() {
        $privatekey = "6LcXtMsSAAAAAGoYIYxdckoQWpt7aoNwYlkoEWWm";
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        return $resp->is_valid;
    }

}

?>