<?php

if (!class_exists("PhpThumbFactory")) {
    include path::plugins("phpthumbfactory/phpthumbfactory.php");
}

class avatar extends app {

    public function __construct() {
        $this->extract($_GET);
        if (isset($_GET["reset"])) {
            $this->reset();
        }
    }

    public function show() {
        switch ($this->tipo) {
            case "participante":
                $participantes = new cparticipantes($this->id);
                if (isset($participantes->foto)) {
                    if (strlen($participantes->foto) > 0) {
                        knife::redirect(uri::root("avatar/" . $participantes->foto));
                    }
                }
                return false;
                break;
        }
    }

    public function save($var) {
        if (!empty($_FILES[$var]["tmp_name"])) {
            $tmp = $_FILES[$var]["tmp_name"];
            $new = uniqid() . ".png";
            $thumb = PhpThumbFactory::create($_FILES[$var]["tmp_name"]);
            $thumb->adaptiveResize(128, 128);
            if ($thumb->save(path::sources("avatar") . "/{$new}", 'png')) {
                return $new;
            }
        }
        return false;
    }

}

?>
