<?php

class ctags extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("tags_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("tags");
        $db->column("nome", $this->get("nome"));
        $db->column("tag", str_replace(" ", "-", strtolower($this->get("nome"))));
        $db->column("destaque", $this->get("destaque"));
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
        }
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqldelete();
            $db->table("tags");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($busca = false, $destaque = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("tags_view");
        $db->column("id");
        $db->column("nome");
        $db->column("tag");
        if ($destaque) {
            $db->match("destaque", "1");
        }
        if ($busca) {
            $db->like("nome", $busca, ($destaque ? "AND" : false));
            $db->like("tag", $busca, "OR");
        }
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>
