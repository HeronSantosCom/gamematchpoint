<?php

class cadm_content_nivel extends app {
    
    private $prefix = false;
    
    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("adm_content_nivel");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    static function lista($adm_content_nivel_id, $busca = false, $ordenacao = 1, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("adm_content_nivel");
        $db->column("id");
        $db->column("titulo");
        $db->morethan("id", $adm_content_nivel_id);
        if ($busca) {
            $db->like("titulo", $busca, "AND");
        }
        $db->order($ordenacao, $ordem);
        return $db->go();
    }


}

?>
