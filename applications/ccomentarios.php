<?php

class ccomentarios extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("comentarios_view");
            $db->column("*");
            $db->column("datacadastro_br", 1, "datacadastro");
            $db->column("datamodificado_br", 1, "datamodificado");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    public function salvar($id = false, $postagens_id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("comentarios");
        $db->column("nome", $this->get("author"));
        $db->column("email", $this->get("email"));
        $db->column("texto", $this->get("comment"));
        $db->column("ip", $_SERVER["REMOTE_ADDR"]);
        $db->column("postagens_id", $postagens_id);
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
        }
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqlsave();
            $db->table("comentarios");
            $db->column("status", '0');
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($postagens_id = false, $busca = false, $registros = false, $inicial = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("comentarios_view");
        $db->column("id");
        $db->column("nome");
        $db->column("email");
        $db->column("texto");
        $db->column("ip");
        $db->column("datacadastro_br", 1, "datacadastro");
        $db->column("datamodificado_br", 1, "datamodificado");
        if ($postagens_id) {
            $db->match("postagens_id", $postagens_id);
        }
        if ($busca) {
            $db->like("nome", $busca, ($postagens_id ? "AND" : false));
            $db->like("email", $busca, "OR");
            $db->like("texto", $busca, "OR");
        }
        $db->order($ordenacao, $ordem);
        if ($registros) {
            if ($inicial) {
                $db->limit($inicial, $registros);
            } else {
                $db->limit($registros);
            }
        }
        return $db->go();
    }

}

?>
