<?php

class cenquetes extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    private function set($name, $value) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"} = $value;
        }
        return $this->$name = $value;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("enquetes_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
                $opcoes = self::lista_opcoes_resposta($id);
                if (!$opcoes) {
                    $opcoes = self::lista_opcoes($id);
                }
                $this->set("opcoes", $opcoes);
                switch ($this->get("tipo")) {
                    case "RADIO":
                        $this->set("tipo_nome", "Escolha única");
                        break;
                    case "CHECK":
                        $this->set("tipo_nome", "Múltipla escolha");
                        break;
                }
            }
        }
    }

    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("enquetes");
        $db->column("titulo", $this->get("titulo"));
        $db->column("questao", $this->get("questao"));
        $db->column("tipo", $this->get("tipo"));
        $db->column("usuarios_id", $_SESSION["usuario"]["id"]);
        if ($id) {
            $db->match("id", $id);
            return $db->go();
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
            if ($db->go()) {
                $id = $db->id();
                if (isset($_POST["opcao"])) {
                    if (is_array($_POST["opcao"])) {
                        foreach ($_POST["opcao"] as $key => $value) {
                            if (strlen(trim($value)) > 0) {
                                $db = new mysqlsave();
                                $db->table("enquetes_opcoes");
                                $db->column("opcao", $value);
                                $db->column("enquetes_id", $id);
                                $db->go();
                            }
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function votar($tipo) {
        $this->extract($_POST, $this->prefix);
        if ($tipo == "RADIO") {
            $db = new mysqlsave();
            $db->table("enquetes_opcoes_resposta");
            $db->column("enquetes_opcoes_id", $this->get("opcao"));
            $db->column("ip", $_SERVER["REMOTE_ADDR"]);
            return $db->go();
        }
        if (isset($_POST["opcao"])) {
            if (is_array($_POST["opcao"])) {
                foreach ($_POST["opcao"] as $opcao) {
                    $db = new mysqlsave();
                    $db->table("enquetes_opcoes_resposta");
                    $db->column("enquetes_opcoes_id", $opcao);
                    $db->column("ip", $_SERVER["REMOTE_ADDR"]);
                    $db->go();
                }
            }
        }
        return true;
    }

    public function finalizar($id = false) {
        if ($id) {
            $db = new mysqlsave();
            $db->table("enquetes");
            $db->column("status", "2");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqlsave();
            $db->table("enquetes");
            $db->column("status", "0");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista_total($busca = false, $ordenacao = 3, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("enquetes_view");
        $db->column("COUNT(*)", false, "total");
        if ($busca) {
            $db->like("titulo", $busca);
            $db->like("questao", $busca, "OR");
        }
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return false;
    }

    static function lista($busca = false, $registros = false, $inicial = false, $ordenacao = 5, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("enquetes_view");
        $db->column("id");
        $db->column("titulo");
        $db->column("questao");
        $db->column("tipo");
        $db->column("datacadastro_br");
        if ($busca) {
            $db->like("titulo", $busca);
            $db->like("questao", $busca, "OR");
        }
        $db->order($ordenacao, $ordem);
        if ($registros) {
            if ($inicial) {
                $db->limit($inicial, $registros);
            } else {
                $db->limit($registros);
            }
        }
        return $db->go();
    }

    static function lista_opcoes($enquetes_id, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("enquetes_opcoes_view");
        $db->column("id");
        $db->column("opcao");
        $db->column("enquetes_titulo");
        $db->column("enquetes_questao");
        $db->column("enquetes_tipo");
        $db->column("IF(0 = 0, 0, 0)", false, "total");
        $db->match("enquetes_id", $enquetes_id);
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

    static function lista_opcoes_resposta($enquetes_id, $ordenacao = 1, $ordem = 'DESC') {
        $db = new mysqlsearch();
        $db->table("enquetes_opcoes_resposta_view");
        $db->column("total");
        $db->column("data_br");
        $db->column("enquetes_opcoes_id", 1, "id");
        $db->column("enquetes_opcoes_opcao", 1, "opcao");
        $db->column("enquetes_titulo");
        $db->column("enquetes_questao");
        $db->column("enquetes_tipo");
        $db->match("enquetes_id", $enquetes_id);
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>
