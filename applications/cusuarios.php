<?php

class cusuarios extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("usuarios_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    public function chat($usuario, $senha = false) {
        $db = new mysqlsearch();
        $db->table("usuarios_view");
        $db->column("*");
        $db->match("usuario", $usuario);
        $db->match("nome", $usuario, "OR");
        if ($senha) {
            $db->match("senha", md5($senha), "AND");
        }
        $db = $db->go();
        if (isset($db[0])) {
            return $db[0];
        }
        return false;
    }

    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("usuarios");
        $db->column("nome", $this->get("nome"));
        $db->column("email", $this->get("email"));
        if (strlen($this->get("usuario")) > 0) {
            $db->column("usuario", $this->get("usuario"));
        }
        if (strlen($this->get("senha")) > 0) {
            $db->column("senha", md5($this->get("senha")));
        }
        if (strlen($this->get("adm_content_nivel_id")) > 0) {
        $db->column("adm_content_nivel_id", $this->get("adm_content_nivel_id"));
        }
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
        }
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqlsave();
            $db->table("usuarios");
            $db->column("status", '0');
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($usuarios_id = false, $adm_content_nivel_id = false, $busca = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("usuarios_view");
        $db->column("id");
        $db->column("nome");
        $db->column("usuario");
        $db->column("email");
        $db->column("adm_content_nivel_titulo");
        $db->morethan("adm_content_nivel_id", $adm_content_nivel_id);
        if ($usuarios_id) {
            $db->match("id", $usuarios_id, "AND", true);
        }
        if ($busca) {
            $db->like("nome", $busca, "AND");
            $db->like("usuario", $busca, "OR");
            $db->like("email", $busca, "OR");
            $db->like("adm_content_nivel_titulo", $busca, "OR");
        }
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>
