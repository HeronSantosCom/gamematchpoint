<?php

class admin extends app {

    public function __construct() {
        $this->extract($_GET);
        if (isset($_GET["reset"])) {
            $this->reset();
        }
        if ($this->content != "enquetes/editar") {
            unset($_SESSION["pool"]["opcoes"]);
        }
    }

    public function login() {
        if (!isset($_SESSION["usuario"])) {
            $this->extract($_POST);
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    $this->extract($_COOKIE);
                }
            }
            if ($this->username) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("usuario", $this->username);
                $usuarios->match("senha", md5($this->password), "AND");
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    if ($this->remember) {
                        setcookie("username", $this->username, time() + 60 * 60 * 24 * 100, "/");
                        setcookie("password", $this->password, time() + 60 * 60 * 24 * 100, "/");
                    }
                    $usuarios[0]["avatar"] = knife::gravatar($usuarios[0]["email"], 60);
                    $_SESSION["usuario"] = $usuarios[0];
                    knife::redirect("painel.html");
                }
                $this->mensagem = $this->html("includes/index/erro.html");
            }
            return false;
        }
        knife::redirect("painel.html");
    }

    public function senha() {
        if (!isset($_SESSION["usuario"])) {
            $this->extract($_POST);
            if ($this->email) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("email", $this->email);
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    $senha = substr(md5(date("r")), -6);
                    $email[] = "Olá {$usuarios[0]["nome"]},";
                    $email[] = "";
                    $email[] = "Como solicitado, estamos enviando sua nova senha de acesso!";
                    $email[] = "Por segurança, anote em um local seguro.";
                    $email[] = "";
                    $email[] = "\tSeu usuário: {$usuarios[0]["usuario"]}";
                    $email[] = "\tSua nova senha é: {$senha}";
                    $email[] = "\tEndereço de acesso: http://www.gamematchpoint.com.br/admin/";
                    $email[] = "";
                    $email[] = "Lembre-se, esta é uma senha temporária, altere se assim desejar!";
                    $email[] = "";
                    $email[] = name;
                    $email[] = "http://www.gamematchpoint.com.br/";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "=============================";
                    $email[] = "Esta é uma mensagem automáticamente, por favor não responder!";
                    $email[] = "IP registrado: {$_SERVER["REMOTE_ADDR"]}";
                    $email[] = "Data de emissão: " . date("r");
                    $email[] = "";
                    $headers = "From: " . strtoupper(name) . " <contato@gamematchpoint.com.br>\n";
                    $headers .= "Reply-To: " . strtoupper(name) . " <contato@gamematchpoint.com.br>\n";
                    if (knife::mail_utf8($usuarios[0]["email"], '[' . strtoupper(name) . '] Sua nova senha!', htmlspecialchars(join("\n", $email)), $headers)) {
                        $save = new mysqlsave();
                        $save->table("usuarios");
                        $save->column("senha", md5($senha));
                        $save->match("id", $usuarios[0]["id"]);
                        $save = $save->go();
                        $this->mensagem = $this->html("includes/senha/sucesso.html");
                        return true;
                    }
                }
                $this->mensagem = $this->html("includes/senha/erro.html");
            }
            return false;
        }
        knife::redirect("painel.html");
    }

    public function logoff() {
        if ($this->logged()) {
            $this->reset();
        }
    }

    public function reset() {
        unset($_SESSION["usuario"]);
        unset($_SESSION["adm_content"]);
        if (isset($_COOKIE["username"])) {
            if (strlen($_COOKIE["username"]) > 0) {
                setcookie("username", false, time() + 60 * 60 * 24 * 100, "/");
                setcookie("password", false, time() + 60 * 60 * 24 * 100, "/");
            }
        }
        knife::redirect("index.html");
    }

    public function content($content = "dashboard") {
        if ($this->logged()) {
            if ($this->content) {
                $content = $this->content;
                $adm_content = new mysqlsearch();
                $adm_content->table("adm_content_view");
                $adm_content->column("*");
                $adm_content->match("content", $content);
                $adm_content->morethan("adm_content_nivel_id", $_SESSION["usuario"]["adm_content_nivel_id"], "AND");
                $adm_content = $adm_content->go();
                if (!isset($adm_content[0])) {
                    $content = "bloqueado.html";
                }
                $_SESSION["adm_content"] = $adm_content[0];
            }
            $this->loaded = $content;
            $this->menu = $this->html("includes/painel/menu.html");
            $this->content = $this->html("includes/painel/content/{$content}.html");
            return true;
        }
    }

    public function logged() {
        if (isset($_SESSION["usuario"])) {
            return true;
        }
        knife::redirect("index.html");
    }

    //
    // Dashboard 
    //

    private function total_postagens() {
        $db = new mysqlsearch();
        $db->table("postagens");
        $db->column("COUNT(*)", false, "total");
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return 0;
    }

    private function total_comentarios() {
        $db = new mysqlsearch();
        $db->table("comentarios");
        $db->column("COUNT(*)", false, "total");
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return 0;
    }

    private function total_comentarios_moderados() {
        $db = new mysqlsearch();
        $db->table("comentarios");
        $db->column("COUNT(*)", false, "total");
        $db->match("status", "0");
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return 0;
    }

    private function total_enquetes() {
        $db = new mysqlsearch();
        $db->table("enquetes_view");
        $db->column("COUNT(*)", false, "total");
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return 0;
    }

    private function total_enquetes_respondidas() {
        $db = new mysqlsearch();
        $db->table("enquetes_opcoes_resposta_view");
        $db->column("SUM(total)", false, "total");
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return 0;
    }

    public function dashboard() {
        if ($this->logged()) {
            $this->total_postagens = $this->total_postagens();
            $this->total_comentarios = $this->total_comentarios();
            $this->total_comentarios_moderados = $this->total_comentarios_moderados();
            $this->total_enquetes = $this->total_enquetes();
            $this->total_enquetes_respondidas = $this->total_enquetes_respondidas();
            $comentarios = ccomentarios::lista($this->id, false, 5, false, 5, "DESC");
            if ($comentarios) {
                foreach ($comentarios as $key => $value) {
                    $comentarios[$key]["avatar"] = knife::gravatar($value["email"], 94);
                }
            }
            $this->comentarios = $comentarios;
        }
    }

    //
    // Menu 
    //
    public function menu() {
        if ($this->logged()) {
            $this->menu = cadm_content::lista($_SESSION["usuario"]["adm_content_nivel_id"], false, true);
        }
    }

    //
    // Listagem na Grid 
    //
    public function lista_usuarios_grid() {
        if ($this->logged()) {
            return cusuarios::lista($_SESSION["usuario"]["id"], $_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function lista_fontes_grid() {
        if ($this->logged()) {
            return cfontes::lista();
        }
    }

    public function lista_participantes_grid() {
        if ($this->logged()) {
            return cparticipantes::lista();
        }
    }

    public function lista_enquetes_grid() {
        if ($this->logged()) {
            return cenquetes::lista();
        }
    }

    public function lista_parceiros_grid() {
        if ($this->logged()) {
            return cparceiros::lista();
        }
    }

    public function lista_postagens_grid() {
        if ($this->logged()) {
            return cpostagens::lista($_SESSION["usuario"]["adm_content_nivel_id"], $_SESSION["usuario"]["id"]);
        }
    }

    public function lista_tags_grid() {
        if ($this->logged()) {
            return ctags::lista();
        }
    }

    public function lista_comentarios_grid() {
        if ($this->logged()) {
            return ccomentarios::lista();
        }
    }

    //
    // Carrega informações para edição
    //
    public function edita_usuario() {
        if ($this->logged()) {
            $usuarios = new cusuarios($this->id);
            if (isset($_POST["submit"])) {
                if ($_POST["senha"] == $_POST["confirma_senha"]) {
                    if ($usuarios->salvar($this->id)) {
                        knife::redirect("painel.html?content=usuarios");
                    } else {
                        $this->mensagem = "Ocorreu um erro ao salvar o usuário.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                    }
                } else {
                    $this->mensagem = "As senhas não conferem!";
                }
            }
            $this->adm_content_nivel = cadm_content_nivel::lista($_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function edita_conta() {
        if ($this->logged()) {
            $usuarios = new cusuarios($_SESSION["usuario"]["id"]);
            if (isset($_POST["submit"])) {
                if ($_POST["senha"] == $_POST["confirma_senha"]) {
                    $_POST["usuario"] = false;
                    $_POST["adm_content_nivel_id"] = false;
                    if ($usuarios->salvar($_SESSION["usuario"]["id"])) {
                        $this->reset();
                    } else {
                        $this->mensagem = "Ocorreu um erro ao salvar suas informações.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                    }
                } else {
                    $this->mensagem = "As senhas não conferem!";
                }
            }
            $this->adm_content_nivel = cadm_content_nivel::lista($_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function edita_fonte() {
        if ($this->logged()) {
            $fontes = new cfontes($this->id);
            if (isset($_POST["submit"])) {
                if ($fontes->salvar($this->id)) {
                    $this->redirect("painel.html?content=fontes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar a fonte.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
        }
    }

    public function edita_participante() {
        if ($this->logged()) {
            $participantes = new cparticipantes($this->id);
            if (isset($_POST["submit"])) {
                if ($participantes->salvar($this->id)) {
                    $this->redirect("painel.html?content=participantes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar o participante.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
        }
    }

    public function edita_enquete() {
        if ($this->logged()) {
            $enquetes = new cenquetes();
            if (isset($_POST["submit"])) {
                if ($enquetes->salvar()) {
                    $this->redirect("painel.html?content=enquetes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar a enquete.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
                $this->edita_enquete_repostas();
            } else {
                if (isset($_POST["adicionar"])) {
                    $this->edita_enquete_repostas(true);
                    $_SESSION["pool"]["opcoes"] = $_SESSION["pool"]["opcoes"] + 1;
                }
            }
            if (!isset($_SESSION["pool"]["opcoes"])) {
                $_SESSION["pool"]["opcoes"] = 1;
            }
            $this->opcoes = false;
            for ($x = 1; $x <= $_SESSION["pool"]["opcoes"]; $x++) {
                $opcoes[$x] = array("id" => $x, "valor" => $this->{"opcao_{$x}"});
            }
            $this->opcoes = $opcoes;
        }
    }

    public function edita_comentario() {
        if ($this->logged()) {
            $comentarios = new ccomentarios($this->id);
            if (isset($_POST["submit"])) {
                if ($comentarios->salvar($this->id)) {
                    $this->redirect("painel.html?content=comentarios");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar o parceiro.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
        }
    }

    public function edita_parceiro() {
        if ($this->logged()) {
            $parceiros = new cparceiros($this->id);
            if (isset($_POST["submit"])) {
                if ($parceiros->salvar($this->id)) {
                    $this->redirect("painel.html?content=parceiros");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar o comentário.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
        }
    }

    public function edita_postagem() {
        if ($this->logged()) {
            $postagens = new cpostagens($this->id, ($_SESSION["usuario"]["adm_content_nivel_id"] > 2 ? $_SESSION["usuario"]["id"] : false));
            if (isset($_POST["submit"])) {
                if ($postagens->salvar($this->id)) {
                    $this->redirect("painel.html?content=postagens");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar a postagem.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
            $this->fontes = cfontes::lista();
            $this->tags = ctags::lista();
        }
    }

    public function edita_tag() {
        if ($this->logged()) {
            $tags = new ctags($this->id);
            if (isset($_POST["submit"])) {
                if ($tags->salvar($this->id)) {
                    $this->redirect("painel.html?content=tags");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar a seção.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
            }
        }
    }

    //
    // Carrega informações para remoção
    //
    public function remove_usuario() {
        if ($this->logged()) {
            $usuarios = new cusuarios($this->id);
            if (isset($_POST["submit"])) {
                if ($_SESSION["usuario"]["id"] != $this->id) {
                    if ($usuarios->apagar($this->id)) {
                        knife::redirect("painel.html?content=usuarios");
                    } else {
                        $this->mensagem = "Ocorreu um erro ao remover o usuário.<br />Tente novamente mais tarde!";
                    }
                } else {
                    $this->mensagem = "Você não tem permissão para excluir este usuário!";
                }
            }
        }
    }

    public function remove_fonte() {
        if ($this->logged()) {
            $fontes = new cfontes($this->id);
            if (isset($_POST["submit"])) {
                $fontes = new cfontes($this->id);
                if ($fontes->apagar($this->id)) {
                    $this->redirect("painel.html?content=fontes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover a fonte.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_participante() {
        if ($this->logged()) {
            $participantes = new cparticipantes($this->id);
            if (isset($_POST["submit"])) {
                $participantes = new cparticipantes($this->id);
                if ($participantes->apagar($this->id)) {
                    $this->redirect("painel.html?content=participantes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover o participante.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_enquete() {
        if ($this->logged()) {
            $enquetes = new cenquetes($this->id);
            if (isset($_POST["submit"])) {
                $enquetes = new cenquetes($this->id);
                if ($enquetes->apagar($this->id)) {
                    $this->redirect("painel.html?content=enquetes");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover a enquete.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_comentario() {
        if ($this->logged()) {
            $comentarios = new ccomentarios($this->id);
            if (isset($_POST["submit"])) {
                $comentarios = new ccomentarios($this->id);
                if ($comentarios->apagar($this->id)) {
                    $this->redirect("painel.html?content=comentarios");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover o comentário.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_parceiro() {
        if ($this->logged()) {
            $parceiros = new cparceiros($this->id);
            if (isset($_POST["submit"])) {
                $parceiros = new cparceiros($this->id);
                if ($parceiros->apagar($this->id)) {
                    $this->redirect("painel.html?content=parceiros");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover o parceiro.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_postagem() {
        if ($this->logged()) {
            $postagens = new cpostagens($this->id);
            if (isset($_POST["submit"])) {
                $postagens = new cpostagens($this->id);
                if ($postagens->apagar($this->id)) {
                    $this->redirect("painel.html?content=postagens");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover a postagem.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    public function remove_tag() {
        if ($this->logged()) {
            $tags = new ctags($this->id);
            if (isset($_POST["submit"])) {
                $tags = new ctags($this->id);
                if ($tags->apagar($this->id)) {
                    $this->redirect("painel.html?content=tags");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover a seção.<br />Tente novamente mais tarde!";
                }
            }
        }
    }

    //
    // Outros
    //
    
    private function edita_enquete_repostas($extract = false) {
        if ($extract) {
            $this->extract($_POST);
        }
        if (isset($_POST["opcao"])) {
            if (is_array($_POST["opcao"])) {
                $_SESSION["pool"]["opcoes"] = false;
                foreach ($_POST["opcao"] as $key => $value) {
                    $this->{"opcao_{$key}"} = $value;
                    $_SESSION["pool"]["opcoes"] = $key;
                }
            }
        }
    }

    public function resultado_enquete() {
        if ($this->logged()) {
            $enquetes = new cenquetes($this->id);
        }
    }

}
