<?php

class inscricao extends app {

    public function __construct() {
        if (isset($_POST["cpf"])) {
            $this->resposta = "1";
            if ($this->check($_POST["cpf"], $_POST["email"])) {
                $ID = $this->save();
                if ($ID) {
                    $email[] = "Inscrição Número: {$ID}";
                    $email[] = "";
                    $email[] = "Dados Pessoais";
                    $email[] = "=============================";
                    $email[] = "Nome Completo: {$_POST["nome"]}";
                    $email[] = "CPF: {$_POST["cpf"]}";
                    $email[] = "Data de Nascimento: {$_POST["data_nascimento"]}";
                    $email[] = "Telefone: {$_POST["telefone"]}";
                    $email[] = "Telefone celular:  {$_POST["celular"]} -  {$_POST["celular_operadora"]}";
                    $email[] = "E-mail: {$_POST["email"]}";
                    $email[] = "Endereço: {$_POST["logradouro"]}";
                    $email[] = "Complemento: {$_POST["complemento"]}";
                    $email[] = "CEP: {$_POST["cep"]}";
                    $email[] = "Bairro: {$_POST["bairro"]}";
                    $email[] = "Cidade: {$_POST["cidade"]}";
                    $email[] = "Estado: {$_POST["uf"]}";
                    $email[] = "";
                    $email[] = "Preferências";
                    $email[] = "=============================";
                    $email[] = "Melhor clube brasileiro (Masc.): {$_POST["clube_masculino"]}";
                    $email[] = "Melhor clube brasileiro (Fem.): {$_POST["clube_feminino"]}";
                    $email[] = "Melhor jogador de Vôlei: {$_POST["jogador_masculino"]}";
                    $email[] = "Melhor jogadora de vôlei: {$_POST["jogador_feminino"]}";
                    $email[] = "Um jogo inesquecível: {$_POST["jogo_inesquecivel"]}";
                    $email[] = "";
                    $email[] = "Pré-entrevista";
                    $email[] = "=============================";
                    $email[] = "O que o voleibol trouxe/trás na sua vida?";
                    $email[] = "{$_POST["voleibol_vida"]}";
                    $email[] = "";
                    $email[] = "O que vale para vencer o Match Point?";
                    $email[] = "{$_POST["vale_vencer"]}";
                    $email[] = "";
                    $email[] = "O que não vale para vencer o Match Point?";
                    $email[] = "{$_POST["nao_vale_vencer"]}";
                    $email[] = "";
                    $email[] = "Você se considera uma pessoa popular? Por que?";
                    $email[] = "{$_POST["e_popular"]}";
                    $email[] = "";
                    $email[] = "O que é mais importante em um virtuality show: Fazer amigos ou vencer o jogo?";
                    $email[] = "{$_POST["mais_importante"]}";
                    $email[] = "";
                    $email[] = "( x ) Li e concordo com o regulamento.";
                    $email[] = "( x ) Autorizo o Game Match Point a entrar em contato comigo e aceito participar de uma pré-seleção.";
                    $email[] = "";
                    $name = htmlspecialchars($_POST['nome']);
                    $mailFrom = htmlspecialchars($_POST['email']);
                    $headers = "From: $name <$mailFrom>\n";
                    $headers .= "Reply-To: $name <$mailFrom>\n";
                    if (mail('felipe@gamematchpoint.com.br', 'Nova Inscrição', htmlspecialchars(join("\n", $email)), $headers)) {
                        $this->resposta = "2";
                    }
                }
            }
        }
    }

    public function check($cpf, $email) {
        $inscricao = new mysqlsearch();
        $inscricao->table("inscricao");
        $inscricao->column("*");
        $inscricao->match("cpf", $cpf);
        $inscricao->match("email", $email, "AND");
        $inscricao = $inscricao->go();
        if (isset($inscricao[0])) {
            return false;
        }
        return true;
    }

    public function save() {
        $db = new mysqlsave();
        $db->table("inscricao");
        $db->column("nome", $_POST['nome']);
        $db->column("cpf", $_POST['cpf']);
        $db->column("data_nascimento", date("Y-m-d", strtotime($_POST['data_nascimento'])));
        $db->column("telefone", $_POST['telefone']);
        $db->column("celular", $_POST['celular']);
        $db->column("celular_operadora", $_POST['celular_operadora']);
        $db->column("email", $_POST['email']);
        $db->column("logradouro", $_POST['logradouro']);
        $db->column("complemento", $_POST['complemento']);
        $db->column("cep", $_POST['cep']);
        $db->column("bairro", $_POST['bairro']);
        $db->column("cidade", $_POST['cidade']);
        $db->column("uf", $_POST['uf']);
        $db->column("clube_masculino", $_POST['clube_masculino']);
        $db->column("clube_feminino", $_POST['clube_feminino']);
        $db->column("jogador_masculino", $_POST['jogador_masculino']);
        $db->column("jogador_feminino", $_POST['jogador_feminino']);
        $db->column("jogo_inesquecivel", $_POST['jogo_inesquecivel']);
        $db->column("voleibol_vida", $_POST['voleibol_vida']);
        $db->column("vale_vencer", $_POST['vale_vencer']);
        $db->column("nao_vale_vencer", $_POST['nao_vale_vencer']);
        $db->column("e_popular", $_POST['e_popular']);
        $db->column("mais_importante", $_POST['mais_importante']);
        $db->column("concordo_regulamento", $_POST['concordo_regulamento']);
        $db->column("autorizo_participar", $_POST['autorizo_participar']);
        if ($db->go()) {
            return $db->id();
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
        }
        return false;
    }

}

?>
