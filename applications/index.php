<?php

class index extends app {

    public function __construct() {
        $this->extract($_GET);
    }

    public function cabecalho() {
        $this->pagina_titulo = name;
        $this->pagina_descricao = "O Game Match Point é o Virtuality Show dos Viciados em Vôlei do Brasil! Inscreva-se e participe de 100 dias de provas e disputas emocionantes em busca dos prêmios do Game Match Point 2.";
        $this->pagina_keyword = name;
        $this->slider = $this->html("includes/index/slider.html");
        $this->menu = $this->html("includes/index/menu.html");
        $this->menu_lateral = $this->html("includes/index/menu_lateral.html");
        $this->popular = $this->html("includes/index/popular.html");
        $this->sites = $this->html("includes/index/sites.html");
    }

    public function conteudo($content = "tag") {
        if (isset($this->content)) {
            $content = $this->content;
        }
        define("conteudo", $this->html("includes/content/{$content}.html"));
    }

    public function menu() {
        $this->menu = ctags::lista(false, true);
    }

    public function menu_lateral() {
        $this->menu_lateral = ctags::lista(false, false);
    }

    public function popular() {
        $popular = cpostagens::lista(false, false, false, 10, false, false, 7, "DESC");
        $this->popular = $popular;
    }

    public function slider() {
        $slider = cpostagens::lista(false, false, false, 6, false, true, 7, "DESC");
        $this->slider = $slider;
    }

    public function parceiros() {
        $this->parceiros = cparceiros::lista();
    }

    public function participantes() {
        $this->participantes = cparticipantes::lista(true);
    }

    private function paginacao($rows, $limit = 10, $sufix = false) {
        $sufix = ($sufix ? " {$sufix}" : null);
        $this->page = (isset($this->page) ? $this->page : 1);
        $this->rows = $rows;
        $this->limit = $limit;
        if ($this->rows > 0) {
            $this->pages = ceil($this->rows / $this->limit);

            if ($this->page > $this->pages) {
                $this->page = $this->pages;
            }

            if ($this->page > 1) {
                $this->first_page = 1;
                $this->previous_page = ($this->page - 1);
            }

            if ($this->page < $this->pages and $this->pages > 1) {
                $this->next_page = ($this->page + 1);
                $this->last_page = $this->pages;
            }

            $this->first_row = (($this->limit * $this->page) - $this->limit);
            $this->last_row = ($this->rows > $this->limit * $this->page ? ($this->limit * $this->page) - 1 : $this->rows - 1);

            $pagination = false;
            for ($x = ($this->pages - 5); $x <= ($this->pages + 5); $x++) {
                if ($x > 0) {
                    if ($x <= $this->pages) {
                        $pagination[] = $x;
                    }
                }
            }
            $this->pagination = $pagination;
        }
    }

    public function postagens() {
        $this->paginacao(cpostagens::lista_total(false, false, $this->tag, false));
        $postagens = cpostagens::lista(false, false, $this->tag, $this->limit, $this->first_row, false);
        if ($postagens) {
            foreach ($postagens as $key => $value) {
                $postagens[$key]["descricao"] = strip_tags($value["descricao"]);
            }
        }
        $this->postagens = $postagens;
    }

    public function enquetes() {
        $this->paginacao(cenquetes::lista_total(false, false, $this->tag, false));
        $enquetes = cenquetes::lista(false, $this->limit, $this->first_row, 5, "DESC");
        $this->enquetes = $enquetes;
    }

    public function enquete() {
        if ($this->id) {
            $enquete = new cenquetes($this->id);
            if (!isset($this->titulo)) {
                $this->redirect("index.html");
            }
            if (isset($_POST["submit"])) {
                if (isset($_POST["opcao"])) {
                    if (crecaptcha::validacao()) {
                        $enquetes = new cenquetes(false, "votacao");
                        if (!$enquetes->votar($this->tipo)) {
                            $this->mensagem = "Ocorreu um erro ao votar a enquete!";
                        } else {
                            $this->mensagem = "Obrigado por votar!";
                        }
                    } else {
                        $this->mensagem = "Validação não está correta!";
                    }
                } else {
                    $this->mensagem = "É necessário especificar uma opção!";
                }
            }
            $opcoes = cenquetes::lista_opcoes($this->id);
            $this->opcoes = $opcoes;
            $this->recaptcha = crecaptcha::form();
            $this->pagina_titulo = "{$this->titulo} - {$this->pagina_titulo}";
            $this->pagina_descricao = html_entity_decode($this->questao);
        }
    }

    public function participante() {
        if ($this->id) {
            $participante = new cparticipantes($this->id);
            if (!isset($this->cpf)) {
                $this->redirect("index.html");
            }
            $this->pagina_titulo = "{$this->nome} - {$this->pagina_titulo}";
            $this->pagina_descricao = html_entity_decode($this->nome);
        }
    }

    public function chat() {
        if (!isset($_SESSION["chat"])) {
            if (isset($_POST["submit"])) {
                if (isset($_POST["sala"])) {
                    if (isset($_POST["apelido"])) {
                        if (isset($_POST["senha"])) {
                            $cusuarios = new cusuarios();
                            $cusuarios = $cusuarios->chat($_POST["apelido"], $_POST["senha"]);
                            if ($cusuarios) {
                                $db = new mysqlsave();
                                $db->table("chat_users");
                                $db->column("apelido", $cusuarios["nome"]);
                                $db->column("ip", $_SERVER["REMOTE_ADDR"]);
                                $db->column("moderador", '1');
                                $db->column("sessao", session_id());
                                $db->column("ultima_acao", date("Y-m-d H:i:s"));
                                $db->column("chat_rooms_id", $_POST["sala"]);
                                if ($db->go()) {
                                    $_SESSION["chat"]["sala"] = $_POST["sala"];
                                    $_SESSION["chat"]["moderador"] = $cusuarios["nome"];
                                } else {
                                    $this->mensagem = "Não foi possível ingressar na sala de bate papo, tente novamente mais tarde!";
                                }
                            }
                        } else {
                            if (crecaptcha::validacao()) {
                                $cusuarios = new cusuarios();
                                if ($cusuarios->chat($_POST["apelido"])) {
                                    $this->apelido = strip_tags($_POST["apelido"]);
                                    $this->moderado = 'sim';
                                    $this->sala = $_POST["sala"];
                                } else {
                                    $db = new mysqlsave();
                                    $db->table("chat_users");
                                    $db->column("apelido", $_POST["apelido"]);
                                    $db->column("ip", $_SERVER["REMOTE_ADDR"]);
                                    $db->column("moderador", '0');
                                    $db->column("sessao", session_id());
                                    $db->column("ultima_acao", date("Y-m-d H:i:s"));
                                    $db->column("chat_rooms_id", $_POST["sala"]);
                                    if ($db->go()) {
                                        $_SESSION["chat"]["sala"] = $_POST["sala"];
                                        $_SESSION["chat"]["visitante"] = $_POST["apelido"];
                                    } else {
                                        $this->mensagem = "Não foi possível ingressar na sala de bate papo, tente novamente mais tarde!";
                                    }
                                }
                            } else {
                                $this->mensagem = "Validação não está correta!";
                            }
                        }
                    } else {
                        $this->mensagem = "É necessário especificar um apelido!";
                    }
                } else {
                    $this->mensagem = "É necessário escolher uma sala!";
                }
            }
            $this->salas = chat::lista_salas();
            $this->recaptcha = crecaptcha::form();
            $this->pagina_titulo = "Chat - {$this->pagina_titulo}";
        }
    }

    public function postagem() {
        if ($this->id) {
            $postagem = new cpostagens($this->id);
            if (!isset($this->titulo)) {
                $this->redirect("index.html");
            }
            $this->descricao = strip_tags($this->descricao);
            $this->usuarios_avatar = knife::gravatar($this->usuarios_email, 120);
            if (isset($_POST["submit"])) {
                if (crecaptcha::validacao()) {
                    $comentarios = new ccomentarios(false, "comentarios");
                    if (!$comentarios->salvar(false, $this->id)) {
                        $this->mensagem = "Ocorreu um erro ao salvar o comentário!";
                    }
                } else {
                    $this->mensagem = "Validação não está correta!";
                }
            }
            $comentarios = ccomentarios::lista($this->id, false, false, false, 6, "DESC");
            if ($comentarios) {
                foreach ($comentarios as $key => $value) {
                    $comentarios[$key]["avatar"] = knife::gravatar($value["email"], 94);
                }
            }
            $this->comentarios = $comentarios;
            $this->recaptcha = crecaptcha::form();
            $this->pagina_titulo = "{$this->titulo} - {$this->pagina_titulo}";
            $this->pagina_descricao = utf8_encode(html_entity_decode($this->descricao));
            $this->pagina_keyword = $this->palavraschaves;
        }
    }

    public function contato() {
        if (isset($_POST["submit"])) {
            if (crecaptcha::validacao()) {
                $this->extract($_POST);
                $email[] = "";
                $email[] = "Mensagem";
                $email[] = "=============================";
                $email[] = "Nome: {$this->name}";
                $email[] = "E-mail: {$this->email}";
                $email[] = "Mensagem: {$this->message}";
                $email[] = "";
                $email[] = "Informações";
                $email[] = "=============================";
                $email[] = "Data: " . date("r");
                $email[] = "IP: {$_SERVER["REMOTE_ADDR"]}";
                $email[] = "";
                $name = htmlspecialchars($this->name);
                $mailFrom = htmlspecialchars($this->email);
                $headers = "From: $name <$mailFrom>\n";
                $headers .= "Reply-To: $name <$mailFrom>\n";
                if (!mail('contato@gamematchpoint.com.br', 'Contato do Site', htmlspecialchars(join("\n", $email)), $headers)) {
                    $this->mensagem = "Não foi possível enviar sua mensagem no momento, tente novamente mais tarde!";
                } else {
                    $this->mensagem = "Mensagem enviada com sucesso!";
                }
            } else {
                $this->mensagem = "Validação não está correta!";
            }
        }
        $this->recaptcha = crecaptcha::form();
    }

    public function rss() {
        //$this->paginacao(cpostagens::lista_total(false, false, $this->tag, false));
        $postagens = cpostagens::lista(false, false, $this->tag, 20, false, false, 11, "DESC");
        if ($postagens) {
            foreach ($postagens as $key => $value) {
                $postagens[$key]["descricao"] = strip_tags($value["descricao"]);
                $postagens[$key]["datacadastro"] = date("r", strtotime($value["datacadastro_db"]));
            }
        }
        $this->rss = $postagens;
    }

}