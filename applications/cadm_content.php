<?php

class cadm_content extends app {
    
    private $prefix = false;
    
    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("adm_content_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    static function lista($adm_content_nivel_id, $busca = false, $main = false, $ordenacao = 3, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("adm_content_view");
        $db->column("id");
        $db->column("content");
        $db->column("titulo");
        $db->column("ordenacao");
        $db->column("adm_content_nivel_titulo");
        $db->morethan("adm_content_nivel_id", $adm_content_nivel_id);
        if ($main) {
            $db->is("adm_content_id", "NULL", "AND");
        }
        if ($busca) {
            $db->like("content", $busca, "AND");
            $db->like("titulo", $busca, "OR");
            $db->like("adm_content_nivel_titulo", $busca, "OR");
        }
        $db->order(4);
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>
