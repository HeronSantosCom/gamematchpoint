<?php

class cparticipantes extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("inscricao_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
                $this->uf = strtoupper($this->uf);
                //if (!$this->foto) {
                //    $this->foto = knife::gravatar($this->email, 128);
                //}
                $ano_hoje = date("Y");
                $ano_nascimento = date("Y", strtotime($this->data_nascimento));
                $this->idade = $ano_hoje - $ano_nascimento;
                $data_aniversario = $ano_hoje . date("-m-d", strtotime($this->data_nascimento));
                if (strtotime(date("Y-m-d")) < strtotime($data_aniversario)) {
                    $this->idade = $this->idade - 1;
                }
            }
        }
    }

    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $avatar = new avatar();
        $avatar = $avatar->save("avatar");
        $db = new mysqlsave();
        $db->table("inscricao");
        $db->column("nome", $this->get("nome"));
        $db->column("cpf", $this->get("cpf"));
        $db->column("data_nascimento", knife::date_converter($this->get("data_nascimento"), "/", "-"));
        $db->column("telefone", $this->get("telefone"));
        $db->column("celular", $this->get("celular"));
        $db->column("celular_operadora", $this->get("celular_operadora"));
        $db->column("email", $this->get("email"));
        $db->column("logradouro", $this->get("logradouro"));
        $db->column("complemento", $this->get("complemento"));
        $db->column("cep", $this->get("cep"));
        $db->column("bairro", $this->get("bairro"));
        $db->column("cidade", $this->get("cidade"));
        $db->column("uf", $this->get("uf"));
        $db->column("clube_masculino", $this->get("clube_masculino"));
        $db->column("clube_feminino", $this->get("clube_feminino"));
        $db->column("jogador_masculino", $this->get("jogador_masculino"));
        $db->column("jogador_feminino", $this->get("jogador_feminino"));
        $db->column("jogo_inesquecivel", $this->get("jogo_inesquecivel"));
        $db->column("voleibol_vida", $this->get("voleibol_vida"));
        $db->column("vale_vencer", $this->get("vale_vencer"));
        $db->column("nao_vale_vencer", $this->get("nao_vale_vencer"));
        $db->column("e_popular", $this->get("e_popular"));
        $db->column("mais_importante", $this->get("mais_importante"));
        $db->column("link_facebook", $this->get("link_facebook"));
        $db->column("link_youtube", $this->get("link_youtube"));
        $db->column("confirmado", "1");
        $db->column("concordo_regulamento", "1");
        $db->column("autorizo_participar", "1");
        if ($avatar) {
            $db->column("foto", $avatar);
        }
        if ($id) {
            $db->match("id", $id);
        }
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqldelete();
            $db->table("inscricao");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($confirmado = false, $busca = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("inscricao_view");
        $db->column("id");
        $db->column("nome");
        $db->column("cpf");
        $db->column("email");
        $db->column("foto");
        $db->column("cidade");
        $db->column("uf");
        $db->column("data_nascimento");
        $db->column("data_nascimento_br");
        if ($busca) {
            $db->like("nome", $busca);
            if ($confirmado) {
                $db->like("confirmado", "1", "AND");
            }
        } else {
            if ($confirmado) {
                $db->like("confirmado", "1");
            }
        }
        $db->order($ordenacao, $ordem);
        $go = $db->go();
        if ($go) {
            foreach ($go as $key => $row) {
                $go[$key]["uf"] = strtoupper($go[$key]["uf"]);
                //if (!$go[$key]["foto"]) {
                //    $go[$key]["foto"] = knife::gravatar($go[$key]["email"], 64);
                //}
                $ano_hoje = date("Y");
                $ano_nascimento = date("Y", strtotime($row["data_nascimento"]));
                $go[$key]["idade"] = $ano_hoje - $ano_nascimento;
                $data_aniversario = $ano_hoje . date("-m-d", strtotime($row["data_nascimento"]));
                if (strtotime(date("Y-m-d")) < strtotime($data_aniversario)) {
                    $go[$key]["idade"] = $go[$key]["idade"] - 1;
                }
            }
        }
        return $go;
    }

}

?>
