<?php

class chat extends app {

    public function __construct() {
        if (isset($_GET["reset"])) {
            unset($_SESSION["chat"]);
        }
    }

    private function apelido() {
        if (isset($_SESSION["chat"])) {
            if (isset($_SESSION["chat"]["moderador"])) {
                $this->apelido = $_SESSION["chat"]["moderador"]; // ["apelido"]
                $this->moderador = true;
            } else {
                $this->apelido = $_SESSION["chat"]["visitante"];
                $this->moderador = false;
            }
            $this->exibe_caixa = "1";
            $this->sala = $_SESSION["chat"]["sala"];
            if ($this->sala == "2") {
                $this->sala_nome = "TORCIDA GAME MATCH POINT";
            } else {
                if (!$this->moderador) {
                    $this->exibe_caixa = false;
                }
                $this->sala_nome = "GAME MATCH POINT";
            }
            return true;
        }
        return false;
    }

    private function posta($sala, $apelido, $mensagem, $moderador, $sistema) {
        $db = new mysqlsave();
        $db->table("chat_board");
        $db->column("apelido", $apelido);
        $db->column("mensagem", $mensagem);
        $db->column("moderador", $moderador);
        $db->column("sistema", $sistema);
        $db->column("ip", $_SERVER["REMOTE_ADDR"]);
        $db->column("sessao", session_id());
        $db->column("chat_rooms_id", $sala);
        if ($db->go()) {
            $db = new mysqlsave();
            $db->table("chat_users");
            $db->column("ultima_acao", date("Y-m-d H:i:s"));
            $db->match("apelido", $apelido);
            $db->match("chat_rooms_id", $sala, "AND");
            $db->go();
        }
    }

    public function kill($sala, $kill) {
        $db = new mysqlsearch();
        $db->table("chat_users");
        $db->column("id");
        $db->column("apelido");
        $db->column("moderador");
        $db->column("chat_rooms_id");
        $db->match("id", $kill);
        $db->match("chat_rooms_id", $sala, "AND");
        $users = $db->go();
        if (isset($users[0]["id"])) {
            $db = new mysqldelete();
            $db->table("chat_users");
            $db->match("id", $users[0]["id"]);
            $db->match("chat_rooms_id", $users[0]["chat_rooms_id"], "AND");
            if ($db->go()) {
                $this->posta($users[0]["chat_rooms_id"], $users[0]["apelido"], "Saiu da sala!", $users[0]["moderador"], true);
            }
        }
    }

    public function entrar() {
        if ($this->apelido()) {
            $_SESSION["chat"]["ultimo_id"] = $this->firstid($this->sala);
            if (!isset($_SESSION["chat"]["logado"])) {
                $mensagem = "Entrou na sala!";
                $this->posta($this->sala, $this->apelido, $mensagem, $this->moderador, true);
            }
            $_SESSION["chat"]["logado"] = true;
        }
    }

    public function sair() {
        if ($this->apelido()) {
            $db = new mysqldelete();
            $db->table("chat_users");
            $db->match("apelido", $this->apelido);
            $db->match("chat_rooms_id", $this->sala, "AND");
            $db->go();
            $this->posta($this->sala, $this->apelido, "Saiu da sala!", $this->moderador, true);
        }
        unset($_SESSION["chat"]);
        $this->redirect("../index.html?content=chat");
    }

    public function processar() {
        if ($this->apelido()) {
            if (isset($_POST["texto"])) {
                $texto = trim(strip_tags($_POST["texto"]));
                if (strlen($texto) > 0) {
                    $this->posta($this->sala, $this->apelido, $texto, $this->moderador, false);
                }
            }
            if ($this->moderador) {
                if (isset($_GET["kill"])) {
                    $kill = trim(strip_tags($_GET["kill"]));
                    $this->kill($this->sala, $kill);
                }
            }
        }
    }

    public function firstid($sala) {
        $db = new mysqlsearch();
        $db->table("chat_board");
        $db->column("id");
        $db->match("chat_rooms_id", $sala);
        $db->order(1, "DESC");
        $db->limit(1);
        $first = $db->go();
        if ($first) {
            return $first[0]["id"];
        }
        return '0';
    }

    public function service_board() {
        if ($this->apelido()) {
            $db = new mysqlsearch();
            $db->table("chat_board");
            $db->column("id");
            $db->column("mensagem");
            $db->column("apelido");
            $db->column("ip");
            $db->column("moderador");
            $db->column("sistema");
            $db->column("data");
            $db->match("chat_rooms_id", $this->sala);
            $db->more("id", $_SESSION["chat"]["ultimo_id"], "AND");
            $board = $db->go();
            if ($board) {
                print "<div id='service_board'>";
                foreach ($board as $line) {
                    print "<div id='linha_{$line["id"]}'>";
                    print "<span class='data'>[" . date("H:i:s", strtotime($line["data"])) . "]</span>";
                    if ($line["moderador"] == "1") {
                        print "<span class='moderador'>" . $line["apelido"] . " (moderador)</span>: ";
                    } else {
                        print "<span class='usuario'>" . $line["apelido"] . "</span>: ";
                    }
                    if ($line["sistema"] == "1") {
                        print "<span class='sistema'>" . $line["mensagem"] . "</span>";
                    } else {
                        print "<span class='mensagem'>" . $line["mensagem"] . "</span>";
                    }
                    print "</div>";
                    $_SESSION["chat"]["ultimo_id"] = $line["id"];
                }
                print "</div>";
            }
        }
    }

    public function service_users() {
        if ($this->apelido()) {
            $db = new mysqlsearch();
            $db->table("chat_users");
            $db->column("id");
            $db->column("apelido");
            $db->column("ip");
            $db->column("moderador");
            $db->column("data");
            $db->match("chat_rooms_id", $this->sala);
            $users = $db->go();
            if ($users) {
                print "<div id='service_users'>";
                foreach ($users as $line) {
                    $moderador = $line["moderador"];
                    $moderador = $line["id"];
                    print "<div id='usuario_{$line["id"]}' class='line'>";
                    print "<span class='" . ($line["moderador"] == "1" ? " moderador " : " usuario ") . ($this->apelido == $line["apelido"] ? ($this->moderador ? " soumoderador " : " soueu ") : null) . "'>";
                    print $line["apelido"];
                    if ($this->moderador and !$line["moderador"]) {
                        if ($this->apelido != $line["apelido"]) {
                            print " <span class='kill'><a href='javascript:void(0);' class='kill' onclick='kill({$line["id"]});'><img src='asset/img/cross.png' alt='Remover!' title='Remover {$line["apelido"]}!' /></a></span>";
                        }
                    }
                    print "</span>";
                    print "</div>";
                }
                print "</div>";
            }
        }
    }

    public function service_cleandb() {
        $db = new mysqlsearch();
        $db->table("chat_users");
        $db->column("apelido");
        $db->column("moderador");
        $db->column("chat_rooms_id");
        $db->morethan("ultima_acao", date("Y-m-d H:i:s", strtotime("-10 minutes")), false, true);
        $users = $db->go();
        if ($users) {
            foreach ($users as $user) {
                $db = new mysqldelete();
                $db->table("chat_users");
                $db->match("apelido", $user["apelido"]);
                $db->match("chat_rooms_id", $user["chat_rooms_id"], "AND");
                if ($db->go()) {
                    $this->posta($user["chat_rooms_id"], $user["apelido"], "Saiu da sala!", $user["moderador"], true); //  por inatividade
                }
            }
        }
    }

    public function service_kill() {
        if ($this->apelido()) {
            $db = new mysqlsearch();
            $db->table("chat_users");
            $db->column("id");
            $db->match("apelido", $this->apelido);
            $db->match("chat_rooms_id", $this->sala, "AND");
            $users = $db->go();
            if (!isset($users[0]["id"])) {
                unset($_SESSION["chat"]);
                print "<script type='text/javascript'>top.document.location.href='../index.html?content=chat';</script>";
            }
        }
    }

    public function service() {
        $this->service_cleandb();
        $this->service_kill();
        $this->service_users();
        $this->service_board();
    }

    static function lista_salas($ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("chat_rooms");
        $db->column("id");
        $db->column("nome");
        $db->column("moderado");
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>