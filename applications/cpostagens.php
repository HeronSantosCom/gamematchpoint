<?php

class cpostagens extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $usuario_id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("postagens_view");
            $db->column("*");
            $db->column("datacadastro_br", 1, "datacadastro");
            $db->column("datamodificado_br", 1, "datamodificado");
            $db->match("id", $id);
            if ($usuario_id) {
                $db->match("usuario_id", $usuario_id, "AND");
            }
            $db = $db->go();
            if (isset($db[0])) {
                $this->pageviews($this->id);
                $this->extract($db[0], $this->prefix);
            }
        }
    }

    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("postagens");
        $db->column("titulo", $this->get("titulo"));
        $db->column("tags_id", $this->get("tags_id"));
        $db->column("texto", $this->get("texto"));
        $db->column("palavraschaves", $this->get("palavraschaves"));
        $db->column("autor", $this->get("autor"));
        $db->column("fontes_id", $this->get("fontes_id"));
        $db->column("usuarios_id", $_SESSION["usuario"]["id"]);
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->column("datacadastro", date("Y-m-d H:i:s"));
        }
        return $db->go();
    }

    public function pageviews($id) {
        $db = new mysqlsave();
        $db->table("postagens");
        $db->column("pageviews", "IF(pageviews = 0, 1, pageviews + 1)");
        $db->match("id", $id);
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqldelete();
            $db->table("postagens");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista_total($adm_content_nivel_id = false, $usuario_id = false, $busca = false, $destaque = false) {
        $db = new mysqlsearch();
        $db->table("postagens_view");
        $db->column("COUNT(*)", false, "total");
        $nivelado = false;
        if ($adm_content_nivel_id > 2) {
            $nivelado = true;
            $db->like("usuarios_id", $usuario_id);
        }
        if ($destaque) {
            $db->is("destaque", "NULL", ($nivelado ? "AND" : false), true);
        } else {
            if ($busca) {
                $db->like("titulo", "%{$busca}%", ($nivelado ? "AND" : false));
                $db->like("descricao", "%{$busca}%", "OR");
                $db->like("texto", "%{$busca}%", "OR");
                $db->like("autor", "%{$busca}%", "OR");
                $db->like("usuarios_usuario", "%{$busca}%", "OR");
                $db->like("usuarios_nome", "%{$busca}%", "OR");
                $db->like("usuarios_email", "%{$busca}%", "OR");
                //$db->like("usuarios_avatar", "%{$busca}%", "OR");
                $db->like("fontes_fonte", "%{$busca}%", "OR");
                $db->like("fontes_nome", "%{$busca}%", "OR");
                //$db->like("fontes_logotipo", "%{$busca}%", "OR");
                $db->like("adm_content_nivel_titulo", "%{$busca}%", "OR");
                $db->like("tags_tag", "%{$busca}%", "OR");
                $db->like("tags_nome", "%{$busca}%", "OR");
            }
        }
        $resultado = $db->go();
        if ($resultado) {
            return $resultado[0]["total"];
        }
        return false;
    }

    static function lista($adm_content_nivel_id = false, $usuario_id = false, $busca = false, $registros = false, $inicial = false, $destaque = false, $ordenacao = 9, $ordem = 'DESC') {
        $db = new mysqlsearch();
        $db->table("postagens_view");
        $db->column("id");
        $db->column("titulo");
        $db->column("descricao");
        $db->column("texto");
        $db->column("autor");
        $db->column("destaque");
        $db->column("pageviews");
        $db->column("palavraschaves");
        $db->column("datacadastro", 1, "datacadastro_db");
        $db->column("datamodificado", 1, "datamodificado_db");
        $db->column("datacadastro_br", 1, "datacadastro");
        $db->column("datamodificado_br", 1, "datamodificado");
        $db->column("usuarios_usuario");
        $db->column("usuarios_nome");
        $db->column("usuarios_email");
        //$db->column("usuarios_avatar");
        $db->column("fontes_fonte");
        $db->column("fontes_nome");
        //$db->column("fontes_logotipo");
        $db->column("adm_content_nivel_titulo");
        $db->column("tags_tag");
        $db->column("tags_nome");
        $nivelado = false;
        if ($adm_content_nivel_id > 2) {
            $nivelado = true;
            $db->like("usuarios_id", $usuario_id);
        }
        if ($destaque) {
            $db->is("destaque", "NULL", ($nivelado ? "AND" : false), true);
        } else {
            if ($busca) {
                $db->like("titulo", "%{$busca}%", ($nivelado ? "AND" : false));
                $db->like("descricao", "%{$busca}%", "OR");
                $db->like("texto", "%{$busca}%", "OR");
                $db->like("autor", "%{$busca}%", "OR");
                $db->like("palavraschaves", "%{$busca}%", "OR");
                $db->like("usuarios_usuario", "%{$busca}%", "OR");
                $db->like("usuarios_nome", "%{$busca}%", "OR");
                $db->like("usuarios_email", "%{$busca}%", "OR");
                //$db->like("usuarios_avatar", "%{$busca}%", "OR");
                $db->like("fontes_fonte", "%{$busca}%", "OR");
                $db->like("fontes_nome", "%{$busca}%", "OR");
                //$db->like("fontes_logotipo", "%{$busca}%", "OR");
                $db->like("adm_content_nivel_titulo", "%{$busca}%", "OR");
                $db->like("tags_tag", "%{$busca}%", "OR");
                $db->like("tags_nome", "%{$busca}%", "OR");
            }
        }
        $db->order($ordenacao, $ordem);
        if ($registros) {
            if ($inicial) {
                $db->limit($inicial, $registros);
            } else {
                $db->limit($registros);
            }
        }
        return $db->go();
    }

//    static function palavraschaves($texto) {
//        $keys = search_keywords::getInstance();
//        $keys = $keys->get_keys();
//
//        if (count($keys)) {
//            echo "Referring URL: $keys[0]<br />Keywords: $keys[1]<br />Search Engine: $keys[2]";
//        }
//    }
}

?>
