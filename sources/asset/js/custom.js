
// TOOLTIPS BEGIN

$( function()
{
    var config =
    {
        // list of element containing cloudy tooltips
        tooltips: 'ul#social li, div.social ul li, div.image',
    };
	
    var cache =
    {
        services:
        {
            odd: false,
            width: 0
        },
		
        tweets:
        {
            width: 0
        },
		
        page_tabs:
        {
            current: null
        },
		
        slider:
        {
            items: 0,
            width: 0,
            total_width: 0
        }
    };

    $(config.jsdepend).addClass('js');
	
    $(config.tooltips).css('z-index', 99999);
	
    $(config.tooltips).hover( function(event)
    {
        $tooltip = $(this).children('div.tooltip');
        $tooltip.show();
    }, function()
    {
        $tooltip = $(this).children('div.tooltip');
        $tooltip.hide();
    });
	
    $(config.tooltips).mousemove( function(event)
    {
        $this = $(this);
        $tooltip = $(this).children('div.tooltip');
		
        $tooltip.css
        ({
            'top': event.pageY - $this.offset().top + 10,
            'left': event.pageX - $this.offset().left - $tooltip.width() + 10
        });
    });
	
    //TOOLTIPS END
	
    // FORM VALIDATION BEGIN
	
    function is_valid_email(email)
    {
        return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email);
    }

    $('#name,#message,#author').blur( function()
    {
        $(this).removeClass('valid');
		
        if ($(this).val() == '')
        {
            $(this).addClass('error');
        } else
{
            $(this).removeClass('error').addClass('valid');
        }
    });
	
        
    $("*[rel=facebox]").facebox ();
    $("*[rel=facebox]").mousedown(function() { 
        var caption = $(this).attr('title'); 
        $().one('reveal.facebox', function() { 
            $('<p class="caption">' + caption + '</p>').appendTo('.content'); 
        }); 
    }); 
    

        
    $('#email').blur( function()
    {
        $(this).removeClass('valid');
		
        if ( ! is_valid_email($(this).val()))
        {
            $(this).addClass('error');
        } else
{
            $(this).removeClass('error').addClass('valid');
        }
    });
	
    $('#comment #submit').click( function()
    {
        $('#name,#message,#email,#author').blur();

        if ($('#comment input.error').length > 0)
        {
            return false;
        }
    });
	
});
	// FORM VALIDATION END