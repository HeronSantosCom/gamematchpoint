$(document).ready(function(){
    
    $("#inscricao").submit(function() {
        var concordo_regulamento = $("#concordo_regulamento").attr("checked");
        var autorizo_participar = $("#autorizo_participar").attr("checked");
        if (concordo_regulamento && autorizo_participar) {
            var empty = false;
            $("input, textarea").each(function() {
                var name = $(this).attr("name");
                if (name != "complemento") {
                    var value = $(this).val();
                    if (value.length == 0) {
                        empty = true;
                    }
                }
            });
            if (!empty) {
                return true;
            }
            alert("Todos os campos do formulário são obrigatórios!");
            return false;
        }
        alert("É necessário estar de acordo com o regulamento e aceitar a participação.");
        return false;
    });
    
    $("#concordo_regulamento").change(function() {
        $("#btn_submit").addClass("disabled");
        var concordo_regulamento = $(this).attr("checked");
        var autorizo_participar = $("#autorizo_participar").attr("checked");
        if (concordo_regulamento && autorizo_participar) {
            $("#btn_submit").removeClass("disabled");
        }
    });
    
    $("#autorizo_participar").change(function() {
        $("#btn_submit").addClass("disabled");
        var concordo_regulamento = $(this).attr("checked");
        var autorizo_participar = $("#concordo_regulamento").attr("checked");
        if (concordo_regulamento && autorizo_participar) {
            $("#btn_submit").removeClass("disabled");
        }
    });
    
    $('#msg-sucesso').modal({
        backdrop: "static",
        show: true
    });
    $('#msg-erro').modal({
        backdrop: "static",
        show: true
    });
    
    $("a[rel=twipsy]").twipsy({
        live: true
    })
    
    $("#fechar-msg-sucesso").click(function() {
        $("#msg-sucesso").modal('hide');
    });
    $("#fechar-msg-erro").click(function() {
        $("#msg-erro").modal('hide');
    });
    

    // POSITION STATIC TWIPSIES
    // ========================

    $(window).bind( 'load resize', function () {
        $(".twipsies a").each(function () {
            $(this)
            .twipsy({
                live: false
                , 
                placement: $(this).attr('title')
                , 
                trigger: 'manual'
                , 
                offset: 2
            })
            .twipsy('show')
        })
    })
});
