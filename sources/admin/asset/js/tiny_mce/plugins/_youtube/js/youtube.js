tinyMCEPopup.requireLangPack();

var YoutubeDialog = {
    init: function () {
        var f = document.forms[0];

        // Get the selected contents as text and place it in the input
        f.youtubeURL.value = tinyMCEPopup.editor.selection.getContent({
            format: 'text'
        });
    },

    insert: function () {
        // Insert the contents from the input into the document
        var url = document.forms[0].youtubeURL.value;
        if (url === null) {
            tinyMCEPopup.close();
            return;
        }
        
        
        var code, regexRes;
        regexRes = url.match("[\\?&]v=([^&#]*)");
        code = (regexRes === null) ? url : regexRes[1];
        if (code === "") {
            tinyMCEPopup.close();
            return;
        }
        
        var pb = '<img src="http://img.youtube.com/vi/' + code + '/hqdefault.jpg" class="mceItem" alt="' + code + '"/>', sep = tinyMCEPopup.editor.getParam('youtube_separator', '<iframe width="560" height="315" src="http://www.youtube.com/embed/' + code + '" frameborder="0" allowfullscreen></iframe>'), pbRE;
        pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {
            return '\\' + a;
        }), 'g');

        tinyMCEPopup.editor.execCommand('mceInsertContent', false, pb);
        
        tinyMCEPopup.editor.onBeforeSetContent.add(function(ed, o) {
            o.content = o.content.replace(pbRE, pb);
        });

        tinyMCEPopup.editor.onPostProcess.add(function(ed, o) {
            if (o.get)
                o.content = o.content.replace(/<img[^>]+>/g, function(im) {
                    if (im.indexOf('class="mceYoutube') !== -1)
                        im = sep;
                    return im;
                });
        });
        
        
        //tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<img src="http://img.youtube.com/vi/' + code + '/hqdefault.jpg" class="mceItem" alt="' + code + '"/>');
        tinyMCEPopup.close();
    }
};

tinyMCEPopup.onInit.add(YoutubeDialog.init, YoutubeDialog);
