var slate = {};

slate = function () {
    var pub = {};
    var self = {};
    var chartColors = ['#263849','#FF9900','#555','#777','#999','#bbb','#ccc','#eee'];

    pub.table = function(obj, source, open, reverse, hide, save) {
        
        var dataTablei28n = {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não foram encontrados resultados",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            }
        };
       
        $('*[rel=datatable]').dataTable ({
            "oLanguage": dataTablei28n
        });
        
        if (reverse == undefined) {
            reverse = false;
        }
        
        if (hide == undefined) {
            hide = false;
        }
        
        if (save == undefined) {
            save = false;
        }
        
        aaSortingId = 0;
        if (hide) {
            aaSortingId = 1;
        }
        
        aaSorting = [[aaSortingId, "asc" ]];
        if (reverse) {
            aaSorting = [[aaSortingId, "desc" ]];
        }
        
        if (hide) {
            $(obj).dataTable ({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": source,
                "sPaginationType": "full_numbers",
                "bStateSave": save,
                "oLanguage": dataTablei28n,
                "aaSorting": aaSorting,
                "fnDrawCallback": function() {
                    $(obj + ' thead tr').each(function() {
                        $(this).children("th:first").hide();
                    });
                    $(obj + ' tbody tr').each(function() {
                        $(this).children("td:first").hide();
                    });
                }
            });
        } else {
            $(obj).dataTable ({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": source,
                "sPaginationType": "full_numbers",
                "bStateSave": save,
                "oLanguage": dataTablei28n,
                "aaSorting": aaSorting
            });
        }

        //
        //var iId = obj + "_hidden";

        //        if (aoColumns == undefined) {
        //            $(obj).dataTable ({
        //                "bProcessing": true,
        //                "sAjaxSource": source,
        //                "sPaginationType": "full_numbers",
        //                "bStateSave": false,
        //                "oLanguage": dataTablei28n,
        //                "aaSorting": aaSorting,
        //                "fnDrawCallback": function() {
        //                    $(obj).val("").remove();
        //                    $(obj + ' tbody tr').each(function() {
        //                        $(this).children("td:first").hide();
        //                    });
        //                }
        //            });
        //        } else {
        //        }

        /* Add events */
        $(obj + ' tbody tr').live('click', function () {
            var nTds = $('td', this);
            var sID = $(nTds[0]).text();
            slate.open(open + sID);
        });
        
    }
    
    pub.open = function (url, target) {
        if (target == undefined) {
            target = "_self"
        }
        window.open(url, target);
    }
    
    pub.question = function (question, url, target) {
        if (target == undefined) {
            target = "_self"
        }
        var agree = confirm(question);
        if (agree) {
            window.open(url, target);
        }
    }

    pub.init = function () {
        
        $('input:text').setMask();
        
        $('#search').find ('input').live ('click' , function () {
            $(this).val ('')
        });
        $("form.form select, form.form input:checkbox, form.form input:radio, form.form input:file").uniform();

        $("*[rel=tooltip]").tipsy ({
            gravity: 's'
        });
        $("*[rel=facebox]").facebox ();

        $('table.stats').each(function() {
            var chartType = '';

            if ( $(this).attr('title') ) {
                chartType = $(this).attr('title');
            } else {
                chartType = 'area';
            }

            var chart_width = $(this).parents ('.portlet').width () * .85;

            $(this).hide().visualize({
                type: chartType,	// 'bar', 'area', 'pie', 'line'
                width: chart_width,
                height: '240px',
                colors: chartColors
            });
        });
    }

    return pub;

}();