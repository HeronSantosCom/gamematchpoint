<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_postagens_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $postagem) {
        $array["aaData"][] = array($postagem["id"], $postagem["titulo"], $postagem["autor"], $postagem["usuarios_nome"]);
    }
}

echo fastjson::convert($array);
?>