<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_participantes_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $participante) {
        $array["aaData"][] = array($participante["id"], $participante["nome"], $participante["cpf"]);
    }
}

echo fastjson::convert($array);
?>