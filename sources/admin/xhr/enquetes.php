<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_enquetes_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $enquete) {
        $array["aaData"][] = array($enquete["id"], $enquete["titulo"]);
    }
}

echo fastjson::convert($array);
?>