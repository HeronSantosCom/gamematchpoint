<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_fontes_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $fonte) {
        $array["aaData"][] = array($fonte["id"], $fonte["nome"], $fonte["fonte"]);
    }
}

echo fastjson::convert($array);
?>