<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_usuarios_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $usuario) {
        $array["aaData"][] = array($usuario["id"], $usuario["usuario"], $usuario["email"], $usuario["nome"], $usuario["adm_content_nivel_titulo"]);
    }
}

echo fastjson::convert($array);
?>