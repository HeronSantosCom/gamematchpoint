<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_comentarios_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $comentario) {
        $array["aaData"][] = array($comentario["id"], $comentario["nome"], $comentario["email"], $comentario["datacadastro"]);
    }
}

echo fastjson::convert($array);
?>