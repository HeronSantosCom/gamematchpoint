<?php

if (!class_exists("fastjson", false)) {
    include path::plugins("fastjson/fastjson.php");
}

$admin = new admin();
$listagem = $admin->lista_tags_grid();

$array["aaData"] = false;
if ($listagem) {
    foreach ($listagem as $tag) {
        $array["aaData"][] = array($tag["id"], $tag["nome"], $tag["tag"]);
    }
}

echo fastjson::convert($array);
?>